public class Lance {

    private Usuario usuarioLance;
    private double valorLance;

    public Lance() {
    }

    public Lance(Usuario usuarioLance, double valorLance) {
        this.usuarioLance = usuarioLance;
        this.valorLance = valorLance;
    }

    public Usuario getUsuarioLance() {
        return usuarioLance;
    }

    public void setUsuarioLance(Usuario usuarioLance) {
        this.usuarioLance = usuarioLance;
    }

    public double getValorLance() {
        return valorLance;
    }

    public void setValorLance(double valorLance) {
        this.valorLance = valorLance;
    }

}
