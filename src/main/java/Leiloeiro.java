public class Leiloeiro {

    private int idLeiloeiro;
    private String nomeLeiloeiro;
    private Leilao leilaoLeiloeiro;

    public Leiloeiro() {
    }

    public int getIdLeiloeiro() {
        return idLeiloeiro;
    }

    public void setIdLeiloeiro(int idLeiloeiro) {
        this.idLeiloeiro = idLeiloeiro;
    }

    public String getNomeLeiloeiro() {
        return nomeLeiloeiro;
    }

    public void setNomeLeiloeiro(String nomeLeiloeiro) {
        this.nomeLeiloeiro = nomeLeiloeiro;
    }

    public Leilao getLeilaoLeiloeiro() {
        return leilaoLeiloeiro;
    }

    public void setLeilaoLeiloeiro(Leilao leilaoLeiloeiro) {
        this.leilaoLeiloeiro = leilaoLeiloeiro;
    }
}
