import java.util.ArrayList;
import java.util.List;

public class Leilao {

    private int idLeilao;
    private List<Lance> listaLances;

    public Leilao() {
        listaLances = new ArrayList<>();
    }

    public int getIdLeilao() {
        return idLeilao;
    }

    public void setIdLeilao(int idLeilao) {
        this.idLeilao = idLeilao;
    }

    public List<Lance> getListaLances() {
        return listaLances;
    }

    public void setListaLances(List<Lance> listaLances) {
        this.listaLances = listaLances;
    }


    public boolean adicionaLance(Lance lance) {

        int tamanhoListaAnterior = listaLances.size();
        int tamanhoListaAtualizada = 0;

        boolean maiorLance = validaMaiorlance(lance, listaLances);

        if (maiorLance) {
            listaLances.add(lance);
            tamanhoListaAtualizada = listaLances.size();
        }

        if (tamanhoListaAtualizada > tamanhoListaAnterior) {
            return true;
        }
        return false;
    }


    public boolean validaMaiorlance(Lance lanceAtual, List<Lance> listaLances) {

        for (Lance lanceAnterior : listaLances) {
            if (lanceAtual.getValorLance() <= lanceAnterior.getValorLance()) {
                throw new RuntimeException("O valor deve ser maior do que o lance anterior");
            }
        }
        return true;
    }

}
