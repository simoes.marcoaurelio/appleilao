import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LeilaoTest {

    private Usuario usuario;
    private Usuario usuario1;
    private Usuario usuario2;
    private Lance lance;
    private Lance lance1;
    private Lance lance2;
    private Leilao leilao;

    @BeforeEach
    public void setup() {

        leilao = new Leilao();

        leilao.setIdLeilao(1);

        usuario1 = new Usuario(1, "Joao");
        lance1 = new Lance(usuario1, 550.00);
        leilao.adicionaLance(lance1);

        usuario2 = new Usuario(2, "Maria");
        lance2 = new Lance(usuario2, 650.00);
        leilao.adicionaLance(lance2);

        usuario = new Usuario();
        lance = new Lance();
    }

    @Test
    public void testaInclusaoDeLanceMaior() {
        usuario.setNomeUsuario("Fernando");
        usuario.setIdUsuario(3);
        lance.setUsuarioLance(usuario);
        lance.setValorLance(850.00);

        boolean incluiuLance = leilao.adicionaLance(lance);

        Assertions.assertEquals(true, incluiuLance);
    }

    @Test
    public void testaInclusaoDeLanceMenor() {
        usuario.setNomeUsuario("Ronaldo");
        usuario.setIdUsuario(4);
        lance.setUsuarioLance(usuario);
        lance.setValorLance(600.00);

        Assertions.assertThrows(RuntimeException.class, () -> {leilao.adicionaLance(lance);});
    }
}
